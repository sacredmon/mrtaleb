import React from 'react';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {InstitutionCardComponent} from "./InstitutionCardComponent";

const tableStyle = {
    margin : {marginTop : 15},
};

export var InstitutionListComponent = (props) => {

    const institutions = props.institutions.map((institution, index) => {
        return (
            <InstitutionCardComponent
                key = {index}
                id = {institution.id}
                name = {institution.name}
                city = {institution.city}
                imgUrl = {institution.imgUrl}
                institutionType = {institution.institutionType}
                history = {props.history}
                remove = {props.remove}
            />
        );
    });

    return (
        <div>
            <div className="panel panel-default" style={tableStyle.margin}>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Institution ID</th>
                            <th>Institution Title</th>
                            <th>City</th>
                            <th>Associated Image</th>
                            <th>Institution Type</th>
                            <th />
                        </tr>
                    </thead>
                    <tbody>
                    {institutions}
                    </tbody>
                </table>
            </div>
        </div>
    );
};


