import React from 'react';
import axios from 'axios';

export var InstitutionCardComponent = (props) => {
  var {id, name, city, imgUrl, institutionType} = props;

  var handleClick = (event) => {
      props.history.push('/institutions/' + id);
      event.preventDefault();
  };

  var handleRemove = (event) => {
    props.remove(event.target.id);
    axios.delete('http://localhost:8081/institutions/' + event.target.id)
    .then(response => {

    })
    .catch(error => {
      console.log(error);
    })
  }

  return (
      <tr>
        <td><a onClick={handleClick}>{id}</a></td>
        <td><a onClick={handleClick}>{name}</a></td>
        <td><a onClick={handleClick}>{city}</a></td>
        <td><a onClick={handleClick}><img src={imgUrl} alt="" width="45"/></a></td>
        <td><a onClick={handleClick}>{institutionType}</a></td>
        <td><span id={id} className="glyphicon glyphicon-remove" onClick={handleRemove}></span></td>
      </tr>
  );

};