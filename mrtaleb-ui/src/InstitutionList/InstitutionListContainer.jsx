import React, {Component} from 'react';
import axios from 'axios';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import {InstitutionListComponent} from "./InstitutionListComponent";

export class InstitutionListContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {institutions: []};
    }

    componentDidMount = () => {
        axios.get('http://localhost:8081/api/institutions')
            .then((response) => {
                this.setState({institutions: response.data});
            })
            .catch((error) => {
                console.log(error);
            });
    };

    removeItem = (index) => {
        const items = this.state.institutions.filter((institution) => {
          return institution.id !== parseInt(index, 10);
        });
        this.setState({ institutions : items });
    }

    render() {
        return (
            <div>
                <InstitutionListComponent institutions={this.state.institutions} history={this.props.history} remove={this.removeItem}/>
            </div>
        );
    }
}