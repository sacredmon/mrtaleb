import React from 'react';
import {NavLink} from 'react-router-dom';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'

export var NavigationComponent = () => {
    return (
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <ul className="nav navbar-nav">
                    <li><NavLink to="/">Home</NavLink></li>
                    <li><NavLink to="/institutions/new">Add an Institution</NavLink></li>
                    <li><NavLink to="/books/new">Add a book</NavLink></li>
                </ul>
            </div>
        </nav>
    );
};