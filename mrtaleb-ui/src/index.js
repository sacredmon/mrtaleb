import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {NavBarComponet} from './NavBar/NavBarComponent';
import {InstitutionListContainer} from './InstitutionList/InstitutionListContainer';

// import {injector} from 'react-services-injector';
// import services from './services';
// injector.register(services);

ReactDOM.render((
        <BrowserRouter>
            <div className="container">
                <NavBarComponet />
                <Switch>
                    <Route exact path="/" component={InstitutionListContainer} />
                    <Route exact path="/institutions" component={InstitutionListContainer} />
                </Switch>
            </div>
        </BrowserRouter>
    ),
    document.getElementById('root'));

