package lt.egzaminas.Model;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "archive")
public class BookArchive extends BookInstitution implements Serializable {

	@Embedded
	private ArchiveTypes archiveType;
	
	public BookArchive() {}
	public BookArchive(String name, 
			String imgUrl, 
			InstitutionTypes institutionType,
			ArchiveTypes archiveType,
			Map<Book, Integer> userProducts) {
		super(name, imgUrl, institutionType, userProducts);
		this.archiveType = archiveType;
	}
	
	public ArchiveTypes getArchiveType() {
		return this.archiveType;
	}
	
	public void setArchiveType(ArchiveTypes archiveType) {
		this.archiveType = archiveType;
	}
	
}
