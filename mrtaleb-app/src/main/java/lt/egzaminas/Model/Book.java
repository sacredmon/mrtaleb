package lt.egzaminas.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
    private long id;
	
	private String title;
	
	private String author;
	
	private int pagination;
	
	private String coverUrl;
	
	private String condition;
	
	private long price;
	
	public Book() {}
	public Book(String title, 
			String author, 
			int pagination, 
			String coverUrl,
			String condition,
			long price) {
		this.title = title;
		this.author = author;
		this.pagination = pagination;
		this.coverUrl = coverUrl;
		this.condition = condition;
		this.price = price;
	}
	
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return this.author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public int getPagination() {
		return this.pagination;
	}
	public void setPagination(int pagination) {
		this.pagination = pagination;
	}
	
	public String getCoverUrl() {
		return this.coverUrl;
	}
	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}
	
	public String getCondition() {
		return this.condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	public long getPrice() {
		return this.price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
}
