package lt.egzaminas.Model;

import javax.persistence.Embeddable;

@Embeddable
public enum ArchiveTypes {
	PUBLIC, RESTRICTED
}
