package lt.egzaminas.Model;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "bookShop")
public class BookShop extends BookInstitution implements Serializable {
	@Embedded
	private AgeGroups ageGroup;

	public BookShop() {
	}

	public BookShop(String name, String imgUrl, InstitutionTypes institutionType, AgeGroups ageGroup,
			Map<Book, Integer> userProducts) {
		super(name, imgUrl, institutionType, userProducts);
		this.ageGroup = ageGroup;
	}

	public AgeGroups getAgeGroup() {
		return this.ageGroup;
	}

	public void setAgeGroups(AgeGroups ageGroup) {
		this.ageGroup = ageGroup;
	}
}
