package lt.egzaminas.Model;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "library")
public class Library extends BookInstitution implements Serializable {
	@Embedded
	private AgeGroups ageGroup;
	
	public Library() {}
	public Library(String name, 
			String imgUrl, 
			InstitutionTypes institutionType,
			AgeGroups ageGroup,
			Map<Book, Integer> userProducts) {
		super(name, imgUrl, institutionType, userProducts);
		this.ageGroup = ageGroup;
	}
	
	public AgeGroups getAgeGroup() {
		return this.ageGroup;
	}
	
	public void setAgeGroups(AgeGroups ageGroup) {
		this.ageGroup = ageGroup;
	}
}
