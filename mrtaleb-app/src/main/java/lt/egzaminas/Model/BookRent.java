package lt.egzaminas.Model;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Embedded;

public class BookRent extends BookInstitution implements Serializable {
	
	@Embedded
	private RentTypes rentType; 
	
	public BookRent() {}
	public BookRent(String name, 
			String imgUrl, 
			InstitutionTypes institutionType,
			RentTypes rentType,
			Map<Book, Integer> userProducts) {
		super(name, imgUrl, institutionType, userProducts);
	}
	
	public RentTypes getRentType() {
		return this.rentType;
	}
	
	public void setRentType(RentTypes rentType) {
		this.rentType = rentType;
	}
	
}
