package lt.egzaminas.Model;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MapKeyJoinColumn;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Institution Type")
public abstract class BookInstitution implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
    private long id;
	
	private String name;
	
	private String imgUrl;
	
	@Embedded
	private InstitutionTypes institutionType;
	
	@ElementCollection
    @CollectionTable(name = "INSTITUTION BOOKS") 
    @MapKeyJoinColumn(name = "BOOK_ID")
    @Column(name="AMOUNT")
    private Map<Book, Integer> userProducts;
	
	public BookInstitution() {}
	
	public BookInstitution(String name, 
			String imgUrl, 
			InstitutionTypes institutionType,
			Map<Book, Integer> userProducts) {
		this.name = name;
		this.imgUrl = imgUrl;
		this.institutionType = institutionType;
		this.userProducts = userProducts;
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getImgUrl() {
		return this.imgUrl;
	}
	
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	public InstitutionTypes getInstitutionType() {
		return this.institutionType;
	}
	
	public void setInstitutionType(InstitutionTypes institutionType) {
		this.institutionType = institutionType;
	}

}
