package lt.egzaminas.Model;

import javax.persistence.Embeddable;

@Embeddable
public enum InstitutionTypes {
	STATE, PRIVATE
}
