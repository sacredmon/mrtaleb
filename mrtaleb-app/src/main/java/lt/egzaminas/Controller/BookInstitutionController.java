package lt.egzaminas.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lt.egzaminas.Model.Book;
import lt.egzaminas.Service.BookInstitutionService;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(value = "/api/users")
@Api(value = "Book Institution")
@CrossOrigin(origins = "http://localhost:3000")
public class BookInstitutionController {

    @Autowired
    private BookInstitutionService bookInstitutionService;
    private final static Logger logger = LogManager.getLogger(BookInstitutionController.class);

    @PostMapping(value = "/{bookinstitution}/books")
    @ApiOperation(value = "Add book to institution", notes = "Adds a book to the institution")
    public ResponseEntity<Void> addToCart(@RequestBody Book book, @PathVariable String institution) {
        if (bookInstitutionService.addToInstitution(book, institution)) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping(value = "/{institution}/books")
    @ApiOperation(value = "Get books from institution", notes = "Returns institutions books")
    public Set<Book> getUserProducts(@PathVariable String institution) {
        return bookInstitutionService.getInstitutionBook(institution);
    }

    @DeleteMapping(value = "/{institution}/books/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete book from institution", notes = "Deletes a book from institution")
    public void deleteFromCart(@PathVariable String institution, @PathVariable String id) {
        bookInstitutionService.deleteFromInstitution(institution, id);
    }

    @PostMapping(value = "/{institution}")
    @ApiOperation(value = "Create Institution", notes = "Creates an institution")
    public void createUser(@PathVariable String institution) {
       bookInstitutionService.createInstitution(institution);
    }
}
