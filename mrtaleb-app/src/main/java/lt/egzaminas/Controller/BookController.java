package lt.egzaminas.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lt.egzaminas.Model.Book;
import lt.egzaminas.Repository.BookRepository;
import lt.egzaminas.Service.BookService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "Book")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/api")
public class BookController {

    @Autowired
    private BookRepository bookRepository;
    
    @Autowired
    private BookService bookService;

    @GetMapping(value = {"/books/{id}"})
    @ApiOperation(value = "Get a book", notes = "Returns a book")
    public Book getBook(@PathVariable String id) {
        return BookService.getBook(id);
    }

    @PostMapping(value = "/books/new")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create books", notes = "Creates a book")
    public void createBook(@RequestBody Book book) {
        bookService.createBook(book);
    }

    @PutMapping(value = "/books/{id}")
    @ApiOperation(value = "Update book", notes = "Updates book info")
    public void updateBook(@RequestBody Book book, @PathVariable String id) {
        bookService.updateBook(book, id);
    }

    @DeleteMapping(value = "/books/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete book", notes = "Delete a single book")
    public void deleteProduct(@PathVariable String id) {
        bookService.deleteBook(id);
    }
}
