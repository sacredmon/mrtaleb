package lt.egzaminas.Service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import lt.egzaminas.Model.Book;
import lt.egzaminas.Repository.BookInstitutionRepository;
import lt.egzaminas.Repository.BookRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private BookInstitutionRepository bookInstitutionRepository;

	@Transactional
	public static Book getBook(@PathVariable String id) {
		return bookRepository.getOne(Long.parseLong(id));
	}

	@Transactional
	public void createBook(@RequestBody Book book) {
		Book newBook = new Book();
		newBook.setTitle(book.getTitle());
		newBook.setAuthor(book.getAuthor());
		newBook.setPagination(book.getPagination());
		newBook.setCoverUrl(book.getCoverUrl());
		newBook.setCondition(book.getCondition());
		newBook.setPrice(book.getPrice());
		bookRepository.save(newBook);
	}

	@Transactional
	public void updateBook(@RequestBody Book book, @PathVariable String id) {
		Book bookThere = bookRepository.getOne(Long.parseLong(id));
		if (book != null) {
			BeanUtils.copyProperties(book, bookThere);
			bookRepository.save(book);
		}
	}

	@Transactional
	public void deleteBook(@PathVariable String id) {
		bookRepository.findAll().forEach(bookInstitution -> bookInstitution.book(id));
		bookInstitutionRepository.delete(Long.parseLong(id));
	}

}
