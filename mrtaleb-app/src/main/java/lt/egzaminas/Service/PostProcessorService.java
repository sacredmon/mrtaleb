package lt.egzaminas.Service;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Service;

import lt.egzaminas.App;

@Service
public class PostProcessorService implements BeanPostProcessor{
	
	final static Logger logger = Logger.getLogger(App.class);

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		logger.info("Initializing Bean: " + bean);
		return bean;
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		logger.info("Destroying Bean: " + bean);
		return bean;
	}

}
