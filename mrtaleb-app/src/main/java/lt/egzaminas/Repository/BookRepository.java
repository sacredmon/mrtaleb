package lt.egzaminas.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import lt.egzaminas.Model.BookInstitution;


public interface BookRepository extends JpaRepository<BookInstitution, Long>{

}
