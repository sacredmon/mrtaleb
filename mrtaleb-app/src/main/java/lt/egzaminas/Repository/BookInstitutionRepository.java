package lt.egzaminas.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import lt.egzaminas.Model.Book;

public interface BookInstitutionRepository extends JpaRepository<Book, Long> {

}
